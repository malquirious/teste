package main

import(
	"net/http"
	"fmt"
)

func getRoot(w http.ResponseWriter, r *http.Request){
	fmt.Fprint(w, "Hello World\n")
}

func getHello(w http.ResponseWriter, r *http.Request){
	nome := r.URL.Query().Get("nome")
	fmt.Fprintf(w, "Hello, %s!\n", nome)
}

func main(){
	http.HandleFunc("/", getRoot)
	
	http.HandleFunc("/hello", getHello)

	_ = http.ListenAndServe(":3333", nil)
}